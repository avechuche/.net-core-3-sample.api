﻿using Microsoft.AspNetCore.Identity;

namespace Sample.API.Common.Models.Identity
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() { }

        public ApplicationRole(string roleName) : base(roleName) { }

        public ApplicationRole(string roleName, string roleDescription) : base(roleName)
        {
            Description = roleDescription;
        }

        public string Description { get; set; }
    }
}
