﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Sample.API.Common.Models.Identity.Managers
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole> roleStore,
            IEnumerable<IRoleValidator<ApplicationRole>> roleValidators, ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber identityErrorDescriber, ILogger<RoleManager<ApplicationRole>> logger) : base(
            roleStore, roleValidators, keyNormalizer, identityErrorDescriber, logger)
        { }
    }
}
