﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Sample.API.Common.Models.Identity.Managers
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> userStore, IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<ApplicationUser> passwordHasher,
            IEnumerable<IUserValidator<ApplicationUser>> userValidators,
            IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber identityErrorDescriber, IServiceProvider serviceProvider,
            ILogger<UserManager<ApplicationUser>> logger) :
            base(userStore, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer,
                identityErrorDescriber,
                serviceProvider, logger)
        { }
    }

}
