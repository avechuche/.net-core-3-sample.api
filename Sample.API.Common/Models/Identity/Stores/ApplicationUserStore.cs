﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Sample.API.Common.IdentityContext;

namespace Sample.API.Common.Models.Identity.Stores
{
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, DatabaseContext>
    {
        public ApplicationUserStore(DatabaseContext dbContext, IdentityErrorDescriber identityErrorDescriber)
            : base(dbContext, identityErrorDescriber)
        { }

    }
}
