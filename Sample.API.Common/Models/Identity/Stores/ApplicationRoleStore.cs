﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Sample.API.Common.IdentityContext;

namespace Sample.API.Common.Models.Identity.Stores
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, DatabaseContext>
    {
        public ApplicationRoleStore(DatabaseContext dbContext, IdentityErrorDescriber identityErrorDescriber)
            : base(dbContext, identityErrorDescriber)
        { }

    }
}
