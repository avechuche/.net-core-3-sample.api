﻿using Microsoft.AspNetCore.Identity;

namespace Sample.API.Common.Models.Identity
{
    public class ApplicationUser : IdentityUser
    { }

}
