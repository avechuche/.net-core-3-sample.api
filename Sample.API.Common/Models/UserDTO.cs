﻿using System;

namespace Sample.API.Common.Models
{
    public class UserDTO
    {
        //[Required(ErrorMessage = "El campo es requerido")]
        public string Id { get; set; }

        //[Required(ErrorMessage = "El campo es requerido")]
        public string Name { get; set; }

        public string Password { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public bool IsEnabled { get; set; }

        public DateTimeOffset? UpdatedOn { get; set; }
    }
}
