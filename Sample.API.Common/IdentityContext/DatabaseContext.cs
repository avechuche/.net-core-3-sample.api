﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Sample.API.Common.Models.Identity;

namespace Sample.API.Common.IdentityContext
{
    //https://stackoverflow.com/questions/48554480/ef-core-get-authenticated-username-in-shadow-properties
    public class DatabaseContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        private readonly IConfiguration _configuration;

        private readonly IHttpContextAccessor _httpContextAccessor;

        public DatabaseContext(DbContextOptions dbContextOptions, IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration)
            : base(dbContextOptions)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("Sample.API");
        }

    }
}

