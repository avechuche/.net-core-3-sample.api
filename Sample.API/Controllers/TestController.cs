﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Sample.API.Controllers
{
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {
        private readonly IOptions<IdentityOptions> _myOptions;

        public TestController(IOptions<IdentityOptions> myOptions)
        {
            _myOptions = myOptions;
        }

        [HttpGet]
        [Route("TestConfigureOptions")]
        public IActionResult TestConfigureOptions()
        {
            _myOptions.Value.Password.RequiredLength = 123;
            return Ok();
        }

    }
}