﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sample.API.Common.IdentityContext;
using Sample.API.Common.Models.Identity;
using Sample.API.Common.Models.Identity.Managers;

namespace Sample.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ApplicationUserManager _applicationUserManager;

        public UserController(ApplicationUserManager applicationUserManager, DatabaseContext db)
        {
            _applicationUserManager = applicationUserManager;
        }

        [HttpGet]
        public IEnumerable<ApplicationUser> Get()
        {
            //return _applicationUserManager.Users.ToList();
            return new List<ApplicationUser>();
        }

    }
}

