﻿using System.Linq;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Sample.API.Common.IdentityContext;

namespace Sample.API.PostConfiguration
{
    public class ConfigureIdentityOptions : IConfigureOptions<IdentityOptions>
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public ConfigureIdentityOptions(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        public void Configure(IdentityOptions identityOptions)
        {
            using (var currentScope = _serviceScopeFactory.CreateScope())
            {
                using (var databaseContext = currentScope.ServiceProvider.GetService<DatabaseContext>())
                {
                    var users = databaseContext.Users.ToList();
                }
            }
        }
    }
}
