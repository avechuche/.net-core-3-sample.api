﻿using System;
using System.IO;
using Microsoft.Extensions.Hosting;
using Sample.API.Common.IdentityContext;
using Microsoft.Extensions.DependencyInjection;
using Sample.API.Common.Models.Identity;
using Sample.API.Common.Models.Identity.Managers;

namespace Sample.API
{
    public static class Seed
    {
        public static IHost SeedData(this IHost webHost)
        {
            using (var currentScope = webHost.Services.CreateScope())
            {
                if (TryCreateDB(currentScope.ServiceProvider))
                {
                    SeedUsers(currentScope.ServiceProvider);
                }
            }

            return webHost;
        }

        public static void SeedUsers(IServiceProvider serviceProvider)
        {
            try
            {
                var userManager = serviceProvider.GetRequiredService<ApplicationUserManager>();

                userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "USERNAME",
                    Email = "USERNAME@GMAIL.COM",
                    EmailConfirmed = true
                }, "PASSWORD123").Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static bool TryCreateDB(IServiceProvider serviceProvider)
        {
            try
            {
                return serviceProvider.GetRequiredService<DatabaseContext>()?.Database?.EnsureCreated() ?? false;
            }
            catch
            {
                return false;
            }
        }

        private static string ReadFile()
        {
            try
            {
                var filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "textstuff.txt");

                using (var inputFile = new StreamReader(filePath))
                {
                    return inputFile.ReadToEnd();
                }
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}


